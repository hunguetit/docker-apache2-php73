FROM ubuntu:20.04
LABEL maintainer = HungNguyen<hungnh@smartosc.com>

# Environments vars
ENV TERM=xterm

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    rm -rf /var/lib/apt/lists/*

RUN add-apt-repository ppa:ondrej/php
RUN apt-get update
RUN apt-get -y upgrade

# Packages installation
RUN DEBIAN_FRONTEND=noninteractive apt-get -y --fix-missing install apache2 \
      php7.3 \
      php7.3-cli \
      php7.3-bcmath \
      php7.3-bz2 \
      php7.3-common \
      php7.3-intl \
      php7.3-gd \
      php7.3-json \
      php7.3-mbstring \
      php7.3-xml \
      php7.3-mysql \
      php7.3-xsl \
      php7.3-zip \
      php7.3-soap \
      php7.3-redis \
      php7.3-memcached \
      php7.3-mongodb \
      php7.3-imagick \
      php7.3-zip \
      php7.3-zmq \
      php7.3-apcu \
      libapache2-mod-php \
      curl \
      php7.3-curl \
      apt-transport-https \
      nano \
      php7.3-xdebug

RUN a2enmod rewrite

RUN apt-get -y install gcc make autoconf libc-dev pkg-config
RUN apt-get -y install libmcrypt-dev
RUN apt-get -y install libmcrypt-dev
RUN apt-get -y install php-pear php7.3-dev libyaml-dev
RUN apt-get -y install php-xml php7.3-xml
RUN mkdir -p /tmp/pear/cache
RUN apt-get -y install gcc make autoconf libc-dev pkg-config
RUN apt-get -y install libmcrypt-dev

RUN update-alternatives --set php /usr/bin/php7.3
RUN pecl channel-update pecl.php.net
RUN pecl install mcrypt-1.0.2

# Composer install
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

# Update the default apache site with the config we created.
ADD config/apache/apache-virtual-hosts.conf /etc/apache2/sites-enabled/000-default.conf
ADD config/apache/apache2.conf /etc/apache2/apache2.conf
ADD config/apache/ports.conf /etc/apache2/ports.conf
ADD config/apache/envvars /etc/apache2/envvars

# Update php.ini
ADD config/php/php.conf /etc/php/7.3/apache2/php.ini

# Install cron
RUN apt-get -y install cron

# Init
ADD init.sh /init.sh
ADD up.sh /var/www/up.sh
RUN chmod 755 /*.sh
RUN chmod 755 /var/www/*.sh

# Add phpinfo script for INFO purposes
RUN echo "<?php phpinfo();" >> /var/www/index.php

RUN chown -R www-data:www-data /var/www

WORKDIR /var/www/

# Volume
VOLUME /var/www

# Ports: apache2, xdebug
EXPOSE 80 9000 22

CMD ["/init.sh", "&&", "up.sh", "&&", "cron", "-f"]
